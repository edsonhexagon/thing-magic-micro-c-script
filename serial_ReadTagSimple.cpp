#include <windows.h>
#include <winnt.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <iostream>

char tag1_Id[12]={0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x68};
char tag2_Id[12]={0x98, 0x76, 0x54 , 0x32, 0x10, 0xfe, 0xdc, 0xba, 0x01, 0x23, 0x45, 0x67};
char tag3_Id[12]={0x10, 0x32, 0x54, 0x76, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x66};
   /* defining the module commands */
    char set_Protocol[7] = {0xFF,0x02, 0x93, 0x00, 0x05, 0x51, 0x7D}; //TAG protocol communication (GEN2)
    char set_Antenna[7] = {0xFF, 0x02, 0x91, 0x01, 0x01, 0x70,0x3B}; // Setting antenna port to port 1
    char set_Region[6] = {0xFF, 0x01, 0x97, 0x01, 0x4B,0xBC}; //Setting region to Australia
    char set_WritePower[7]= {0xFF,  0x02,  0x94, 0x0B,  0xB8,  0x2A,  0x27};
    char set_ReadPower[7]= {0xFF,  0x02,  0x92,  0x0B,  0xB8,  0x4A,  0xE1};
    char get_EPC[8] = {0xFF, 0x03, 0x21, 0x03, 0xE8, 0x00, 0xA5, 0xE8}; // Read the tag ID
    char get_Temperature[5]={0xFF, 0x00, 0x72, 0x1D, 0x7D};
    char writeUserData[30] = {0xFF,0x19,0x24,0x03,0xE8,0x88,0x00,0x00,0x00,0x00,0x00,0x03,
                          0xFF,0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                          0xFF,0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,0x0C,0xBC};
    char getepc_recv[28];
    char tagdetected[12];
    char last_tagdetected[12];
    char protocol_recv[7];
    char antena_recv[7];
    char region_recv[7];
    char readpower_recv[7];
    char writepower_recv[7];
    char gettemp[8];
    char writedata_recv[7];
    char cleanSerial[256];




HANDLE hSerial;
using std::cout;

int main(int)
{
 
 

    //Declare variables and structures
    DCB dcbSerialParams = {0};
    COMMTIMEOUTS timeouts = {0};
    DWORD bytes_written, total_bytes_written,dwReading = 0;


    fprintf(stderr, "Opening serial port...");
     hSerial = CreateFileA(
                "COM7", GENERIC_READ|GENERIC_WRITE, 0, NULL,
                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
    if (hSerial == INVALID_HANDLE_VALUE)
    {
            fprintf(stderr, "Error opening serial. Try on connecting on COM X\n");
    }
    else fprintf(stderr, "Opening serial successful \n");
     
    // Set device parameters (9600 baud, 1 start bit,
    // 1 stop bit, no parity)

 dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
    if (GetCommState(hSerial, &dcbSerialParams) == 0)
    {
        fprintf(stderr, "Error getting device state\n");
        CloseHandle(hSerial);
    }
     
    dcbSerialParams.BaudRate = CBR_9600;
    dcbSerialParams.ByteSize = 8;
    dcbSerialParams.StopBits = ONESTOPBIT;
    dcbSerialParams.Parity = NOPARITY;
    
    if(SetCommState(hSerial, &dcbSerialParams) == 0)
    {
        fprintf(stderr, "Error setting device parameters\n");
        CloseHandle(hSerial);
    }
    // Set COM port timeout settings
    timeouts.ReadIntervalTimeout = 50;
    timeouts.ReadTotalTimeoutConstant = 50;
    timeouts.ReadTotalTimeoutMultiplier = 10;
    timeouts.WriteTotalTimeoutConstant = 50;
    timeouts.WriteTotalTimeoutMultiplier = 10;
    if(SetCommTimeouts(hSerial, &timeouts) == 0)
    {
        fprintf(stderr, "Error setting timeouts\n");
        CloseHandle(hSerial);
        return 1;
    }
   // ----------------------- Set the Communication Protocol----------------------------------
    fprintf(stderr, "Configurating protocol...");
    Sleep(50);
    ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
    if(!WriteFile(hSerial, set_Protocol, 7, &bytes_written, NULL))
    {
        fprintf(stderr, "Error configuring protocol\n");
        CloseHandle(hSerial);
    }   
        for(int i=0;i<6;i++)
        {
            ReadFile(hSerial,&protocol_recv[i],1,&dwReading,NULL);
        }


if (protocol_recv[1] != 0x00 && protocol_recv[2] != 0x93 && protocol_recv[3] != 0x00 && protocol_recv[4] != 0x00)
{
    fprintf(stderr, "Protocol configuration Error\n");
}
else fprintf(stderr, "Protocol configuration success\n");

//-----------------------------------------------------------------------------------///


   // ----------------------- Set Antenna port----------------------------------
    fprintf(stderr, "Configurating antenna port...");
    Sleep(50);
    ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
    if(!WriteFile(hSerial, set_Antenna, 7, &bytes_written, NULL))
    {
        fprintf(stderr, "Error configuring antenna\n");
        CloseHandle(hSerial);
    }   
        for(int i=0;i<6;i++)
        {
            ReadFile(hSerial,&antena_recv[i],1,&dwReading,NULL);
        }


if (antena_recv[1] != 0x00 && antena_recv[2] != 0x91 && antena_recv[3] != 0x00 && antena_recv[4] != 0x00)
{
    fprintf(stderr, "Antenna configuration Error\n");
}
else fprintf(stderr, "Antenna configuration success\n");

//-----------------------------------------------------------------------------------///



   // ----------------------- Set Region of Operation (Default to Australia)----------------------------------
    fprintf(stderr, "Configurating region of operation...");
    Sleep(50);
    ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
    if(!WriteFile(hSerial, set_Region, 6, &bytes_written, NULL))
    {
        fprintf(stderr, "Error configuring region\n");
        CloseHandle(hSerial);
    }   
        for(int i=0;i<6;i++)
        {
            ReadFile(hSerial,&region_recv[i],1,&dwReading,NULL);
        }


if (region_recv[1] != 0x00 && region_recv[2] != 0x97 && region_recv[3] != 0x00 && region_recv[4] != 0x00)
{
    fprintf(stderr, "Region configuration Error\n");
}
else fprintf(stderr, "Region configuration success\n");

//-----------------------------------------------------------------------------------///

   // ----------------------- Set Read Power to 30dbm----------------------------------
    fprintf(stderr, "Configurating read power to 30dbm...");
    Sleep(50);
    ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
    if(!WriteFile(hSerial, set_ReadPower, 7, &bytes_written, NULL))
    {
        fprintf(stderr, "Error setting read power\n");
        CloseHandle(hSerial);
    }   
        for(int i=0;i<6;i++)
        {
            ReadFile(hSerial,&readpower_recv[i],1,&dwReading,NULL);
        };


if (readpower_recv[1] != 0x00 && readpower_recv[2] != 0x92 && readpower_recv[3] != 0x00 && readpower_recv[4] != 0x00)
{
    fprintf(stderr, "Read power configuration Error\n");
}
else fprintf(stderr, "Read power configuration success\n");

//-----------------------------------------------------------------------------------///



 // ----------------------- Set Write Power to 30dbm----------------------------------
    fprintf(stderr, "Configurating write power to 30dbm...");
    Sleep(50);
    ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
    if(!WriteFile(hSerial, set_WritePower, 7, &bytes_written, NULL))
    {
        fprintf(stderr, "Error setting write power\n");
        CloseHandle(hSerial);
    }   
        for(int i=0;i<6;i++)
        {
            ReadFile(hSerial,&writepower_recv[i],1,&dwReading,NULL);
        }


if (writepower_recv[1] != 0x00 && writepower_recv[2] != 0x94 && writepower_recv[3] != 0x00 && writepower_recv[4] != 0x00)
{
    fprintf(stderr, "Write power configuration Error\n");
}
else fprintf(stderr, "Write power configuration success\n");


//-----------------------------------------------------------------------------------///

    //Module configuration is finished. Now we start to read tags
    while(true)
    {
        //Trying to locate some tag
        fprintf(stderr, "\nSearching for tags...\n");
        bool teste = 0;
        while(teste ==0)
        {
        //before, lets read the module temperature
         fprintf(stderr, "Module Temperature is:");
         ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
         Sleep(50);
          if(!WriteFile(hSerial, get_Temperature, 5, &bytes_written, NULL))
            {
                fprintf(stderr, "Error trying to read temperature\n");
            }
            else
            {
                for(int i=0;i<7;i++)
                {
                    ReadFile(hSerial,&gettemp[i],1,&dwReading,NULL);
                }
                if(gettemp[0]==-1 && gettemp[3]==0x00 && gettemp[4]==0x00)
                {
                     fprintf(stderr,"%d Celsius degree \n",gettemp[5]); 
                }
            }
            Sleep(50);
            ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
            memset(getepc_recv, 0, 28);            
            if(!WriteFile(hSerial, get_EPC, 8, &bytes_written, NULL))
            {
                fprintf(stderr, "Error trying to read EPC\n");
                CloseHandle(hSerial);
            }   
            
            for(int i=0;i<22;i++)
            {
                ReadFile(hSerial,&getepc_recv[i],1,&dwReading,NULL);
            }
            //if a tag was read correctly,saves its ID
            if(getepc_recv[0]==-1 && getepc_recv[3]==0x00 && getepc_recv[4]==0x00)
            {
                fprintf(stderr, "Tag EPC found:\n"); //Confirmar o IF abaixo se começa de 6 ou 5
                for(int i=6; i<18;i++)
                {
                    printf("%X",(unsigned char) getepc_recv[i]);
                    tagdetected[i-6]=getepc_recv[i];
                }
                 printf("\n");
                //compare the tag id read with the values known and with the last tag read
                int cmp1,cmp2,cmp3,cmp4=0;
                for(int i =0; i<12; i++)
                {
                    if((tagdetected[i] == tag1_Id[i])) cmp1=1;
                    else cmp1=0;
                }
                for(int i =0; i<12; i++)
                {
                    if((tagdetected[i] == tag2_Id[i])) cmp2=1;
                    else cmp2=0;
                }
                for(int i =0; i<12; i++)
                {
                    if((tagdetected[i] == tag3_Id[i])) cmp3=1;
                    else cmp3=0;
                }
                for(int i =0; i<12; i++)
                {
                    if((tagdetected[i] != last_tagdetected[i])) cmp4=1;
                    else cmp4=0;
                }                

                if((cmp1 || cmp2 || cmp3)&&(cmp4)) 
                {
                    teste = 1;
                    bool teste2 = 0;
                    for(int i=0; i<5; i++)
                    {
                    //if the tag read is known, write data (16bytes 0xFF) to its memory
                        ReadFile(hSerial,&cleanSerial,256,&dwReading,NULL); //cleaning serial
                        WriteFile(hSerial, writeUserData, 30, &bytes_written, NULL);
                        Sleep(200);
                        for(int i=0;i<6;i++)
                        {
                            ReadFile(hSerial,&writedata_recv[i],1,&dwReading,NULL);
                        }
                        //if writing data was successful, break out;
                        char dummy_test = writedata_recv[0];
                        if(writedata_recv[0]==-1 && writedata_recv[3]==0x00 && writedata_recv[4]==0x00)
                        {
                                for(int j=0;j<12;j++)
                                {
                                    last_tagdetected[j] = tagdetected[j];
                                }
                            fprintf(stderr, "Data wrote to tag memory!\n");
                            i=5;
                        }
                        else if(i==6)fprintf(stderr, "Failed to write data to memory\n");
                    }
                }

            }
            Sleep(500);//if did not find any tag,the module rests for 0.5seconds
        }
        Sleep(500); //if finding a tag and writing data to its memory was successful, then turn sleeps for 2sec
    }

    // Close serial port
    fprintf(stderr, "Closing serial port...");
    if (CloseHandle(hSerial) == 0)
    {
        fprintf(stderr, "Error\n");
        return 1;
    }
    fprintf(stderr, "OK\n");
 
    return 0;
}
