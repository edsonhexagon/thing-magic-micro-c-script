/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

using namespace std;

uint8_t msg[]={0xFF,0x21,0x24,0x03,0xe8,
0x01,0x00,0x00,0x00,0x02,0x03,0x00,0x00,0x00,0x00,
0x60,0x01,0x23,0x45,0x67,0x89,0xAB,0xCD,0xEF,0x01,
0x23,0x45,0x67,0x11, 0x11, 0x22, 0x22, 0x00, 0x00,
0x00,0x00};
/*uint8_t msg[]={
0xFF,0x17,0x24,0x03,0xE8,0x03,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x20,0x10,0x12,0x34,
0x11,0x11,0x22,0x22
};*/

static uint16_t crctable[] =
{
  0x0000, 0x1021, 0x2042, 0x3063,
  0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b,
  0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
};

uint16_t calculateCRC(uint8_t *u8Buf, uint8_t len)
{
  uint16_t crc = 0xFFFF;

  for (uint8_t i = 0 ; i < len ; i++)
  {
    crc = ((crc << 4) | (u8Buf[i] >> 4)) ^ crctable[crc >> 12];
    crc = ((crc << 4) | (u8Buf[i] & 0x0F)) ^ crctable[crc >> 12];
  }

  return crc;
}


int main()
{
    cout<<"Your CRC is: \n";
    
    uint16_t crc=calculateCRC(&msg[1],msg[1]+2);
    cout<<hex<<crc;
    return 0;
}
